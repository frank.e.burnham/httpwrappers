package httpwrappers

import (
	"encoding/json"
	"net/http"
)

// HTTPResponse provides structure for returning an HTTP response
// it is used for predictability
type HTTPResponse struct {
	Status int
	Body   interface{}
}

// WrapJSON marshalls the return value from to json
func WrapJSON(handler func(*http.Request) HTTPResponse) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		data := handler(r)

		w.WriteHeader(data.Status)

		marshalled, err := json.Marshal(data.Body)
		handleError(w, err)

		w.Header().Set("Content-Type", "application/json")

		_, err = w.Write(marshalled)
		handleError(w, err)

		return
	}
}
