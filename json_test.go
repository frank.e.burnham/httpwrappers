// test to check the status code is set as expected
// add example!

package httpwrappers

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

type testBody struct {
	Name string `json:"name"`
	ID   int    `json:"id"`
}

func makeResponseFunc(status int, body interface{}) func(*http.Request) HTTPResponse {
	return func(r *http.Request) HTTPResponse {
		return HTTPResponse{
			Status: status,
			Body:   body,
		}
	}
}

func TestWrapJSONContentTypeHeader(t *testing.T) {
	wrappedFunc := WrapJSON(makeResponseFunc(http.StatusOK, nil))
	recorder := httptest.NewRecorder()

	wrappedFunc(recorder, &http.Request{})

	if headerCount := len(recorder.HeaderMap["Content-Type"]); headerCount != 1 {
		t.Logf("%s: Expected 1 Content-Type got %d", failX, headerCount)
		t.Fail()

		return
	}

	if header := recorder.HeaderMap["Content-Type"][0]; header != "application/json" {
		t.Logf("%s: Got Content-Type %d expected applicaiton/json",
			failX,
			header)
		t.Fail()

		return // there was a right way to do this
	}

	t.Logf("%s: Content-Type header is set", passCheck)
}

func TestWrapJSONBody(t *testing.T) {
	expected_body := testBody{
		Name: "test",
		ID:   1,
	}

	wrappedFunc := WrapJSON(makeResponseFunc(http.StatusOK, expected_body))
	recorder := httptest.NewRecorder()

	wrappedFunc(recorder, &http.Request{})

	actual_body := testBody{}
	body := recorder.Body.Bytes()
	if err := json.Unmarshal(body, &actual_body); err != nil {
		t.Log(err)
		t.Fail()

		return
	}

	if actual_body != expected_body {
		t.Logf("%s: Incorrect body. Expected %s got %s",
			failX,
			expected_body,
			actual_body)
		t.Fail()

		return
	}

	t.Logf("%s: Body is marshaled correctly", passCheck)
}

func TestWrapJSONStatusCode(t *testing.T) {
	wrappedFunc := WrapJSON(makeResponseFunc(http.StatusInternalServerError, nil))
	recorder := httptest.NewRecorder()

	wrappedFunc(recorder, &http.Request{})

	if recorder.Code != http.StatusInternalServerError {
		t.Logf("%s: Incorrect status code. Expected 500 got %d",
			failX,
			recorder.Code)
		t.Fail()

		return
	}

	t.Logf("%s: Correctly set status code", passCheck)
}
