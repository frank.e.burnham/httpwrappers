package httpwrappers

import (
	"log"
	"net/http"
)

// handleError takes a response writer and an error
// and writes error information to the log and writer
// if the error is not nil
func handleError(w http.ResponseWriter, err error) {
	if err != nil {
		log.Print(err.Error())
		http.Error(w, "Internal Error", http.StatusInternalServerError)
	}

	return
}
